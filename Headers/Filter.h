#ifndef FILTER_H
#define FILTER_H

#include<stdio.h>
#include<iostream>
#include<cmath>

#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>

#include"ImageProcess.h"


namespace Filter{
    cv::Mat GaussianBlur(cv::Mat src, cv::Size ksize, double sigmaX, double sigmaY = (0.0), int borderType = 4);

    cv::Mat GaussianBandFreqFilter(cv::Mat srcSpectrum, double sigmaX, double sigmaY, double centX, double centY);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, double X, double Y, double width, double height);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, Corner LedtUP, double width, double height);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, cv::Rect Filter);
};

#endif