# What is this?

`PatternRecognition` is a package implementing pattern recognition work for the [Gantry control software in IHEP](https://gitlab.cern.ch/ihep-mdoule-assembly/GantryControlSoftware).

# What does it depend on?

The program is developed on macOS and vscode, with c++ and opencv.

- [ ] System info: M1 macOS Ventura 13.0;

- [ ] cmake: version 3.26.4;

- [ ] opencv: 4.7.0_4 

- [ ] c++ compiler: Apple clang version 14.0.0 (clang-1400.0.29.202)

I believe this would work with WSL and other Linux system.

# How to use it?

To clone it, go into the repo folder and do a

```
cmake .
make
```

Then every executable file is under the `build/` folder, just enter `./build/[macros]` to run the code! 

If you also use vscode to develope this program, it is better to inspect `.vscode/` folder and change the files to match your system!

# How does it work?
The [Headers](Headers) and [src](src) folder has the headers for the program. Use the headers! This will bring convience for merging with the full control software. And use the headers will keep the macros tide!

The [images](images) folder includes the images during development. 

In the [util](util) folder, you can find a wide set of macros used during developement period, which should serve as a useful reference for developing new macros.

The [GantryWorkingLog.pdf](GantryWorkingLog.pdf) is a log to record progress of this program. Remember to update it when you have any progress.

## What are the headers?

As the name shows, the headers have relative functions to finish all the work.

`ImageLoading` - read images;

`ImageProcess` - basic image process, incluing binary, display, spectrum, corner recognition...

`Filter` - includes many filters, gaussian blur, gaussian core...

# What to do if I have other question?

Please email to xinhui.huang@cern.ch, contact with the developer (xinhui)
