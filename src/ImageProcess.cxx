#include "ImageProcess.h"
namespace ImageProcess{
    double getImageMeanGradValue(cv::Mat src, int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType){
        cv::Mat dst_Sobel;
        // :: Tenegrad - mean gray value ::
        cv::Sobel(src, dst_Sobel, ddepth, dx, dy, ksize, scale, delta, borderType);
        double meanGrayValue = 0.0;
        meanGrayValue = cv::mean(dst_Sobel)[0];

        return meanGrayValue;
    };
    
    double getImageMeanGrayValue(cv::Mat src){
        cv::Mat meanValueImage;
	    cv::Mat meanStdValueImage;
	    cv::meanStdDev(src, meanValueImage, meanStdValueImage);
	    double meanGrayValue = 0.0;
	    meanGrayValue = cv::mean(meanValueImage)[0];

        return meanGrayValue;
    };
    
    double getImageMeanStdValue(cv::Mat src){
        cv::Mat meanValueImage;
	    cv::Mat meanStdValueImage;
	    cv::meanStdDev(src, meanValueImage, meanStdValueImage);
	    double meanStdDev = 0.0;
	    meanStdDev = cv::mean(meanStdValueImage)[0];

        return meanStdDev;
    };

    cv::Mat getBinaryImage(cv::Mat src, double thres_value, double maxval, int type){
        cv::Mat binaryGrayImage;
        cv::threshold(src, binaryGrayImage, thres_value, maxval, type); 
        return binaryGrayImage;
    };

    cv::Mat getSpectrumDFT(cv::Mat src){
        cv::Mat dst = {}, src_Gray = {};
        std::cout<<"0000"<<std::endl;
        std::cout<<src.type()<<std::endl;   
        if (src.type() >= 8){
            //src.convertTo(src, src.type()-8);
            std::cout<<"multi channel image, convert to gray image"<<std::endl;
            cv::cvtColor(src, src_Gray, cv::COLOR_RGB2GRAY);
        }
        else{
            std::cout<<"Gray image"<<std::endl;
            src_Gray = src;
        }
        std::cout<<"src_Gray.type(): "<<src_Gray.type()<<std::endl;   
        cv::Mat transformMat = {}, imageSpectrum = {};

        cv::Mat planes[] = {cv::Mat_<float>(src_Gray), cv::Mat::zeros(src_Gray.size(), CV_32F)};
        cv::merge(planes, 2, transformMat);
        
        cv::dft(transformMat, transformMat);
        cv::split(transformMat, planes);
        cv::magnitude(planes[0], planes[1], imageSpectrum);
        imageSpectrum += cv::Scalar(1);
        log(imageSpectrum, imageSpectrum);
        cv::normalize(imageSpectrum, imageSpectrum, 0, 1, cv::NORM_MINMAX);
        imageSpectrum = imageSpectrum(cv::Rect(0, 0, imageSpectrum.cols & -2, imageSpectrum.rows & -2));
        int cx = imageSpectrum.cols / 2;
        int cy =imageSpectrum.rows / 2; 
        cv::Mat q0(imageSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(imageSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(imageSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(imageSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);

        dst = imageSpectrum;
        return dst;
    };

    ComplexSpectrum getComplexSpectrum(cv::Mat src){
        ComplexSpectrum complexSpectrum;
        cv::Mat dst, src_Gray;
        if (src.type() >= 8){
            //src.convertTo(src, src.type()-8);
            std::cout<<"multi channel image, convert to gray image"<<std::endl;
            cv::cvtColor(src, src_Gray, cv::COLOR_RGB2GRAY);
        }
        else{
            std::cout<<"Gray image"<<std::endl;
            src_Gray = src;
        }
        
        cv::Mat transformMat = {}, imageSpectrum = {};
        cv::Mat planes[] = {cv::Mat_<float>(src_Gray), cv::Mat::zeros(src_Gray.size(), CV_32F)};
        cv::merge(planes, 2, transformMat);
        cv::dft(transformMat, transformMat);
        cv::split(transformMat, planes);

        complexSpectrum.transformMat = transformMat;
        complexSpectrum.x = planes[0];
        complexSpectrum.y = planes[1];
        cv::magnitude(planes[0], planes[1], complexSpectrum.magnitude);
        cv::phase(planes[0], planes[1], complexSpectrum.phase);

        cv::magnitude(planes[0], planes[1], imageSpectrum);
        imageSpectrum += cv::Scalar(1);
        log(imageSpectrum, imageSpectrum);
        cv::minMaxLoc(imageSpectrum, &complexSpectrum.mag_min, &complexSpectrum.mag_max);
        //std::cout << complexSpectrum.mag_max <<", "<<complexSpectrum.mag_min<<std::endl;
        cv::normalize(imageSpectrum, imageSpectrum, 0, 1, cv::NORM_MINMAX);
        imageSpectrum = imageSpectrum(cv::Rect(0, 0, imageSpectrum.cols & -2, imageSpectrum.rows & -2));
        int cx = imageSpectrum.cols / 2;
        int cy =imageSpectrum.rows / 2; 
        cv::Mat q0(imageSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(imageSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(imageSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(imageSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);
        dst = imageSpectrum;

        return complexSpectrum;
    };


    cv::Mat getSpectrumiDFT(cv::Mat srcSpectrum, ComplexSpectrum complexSpectrum, int nfactor){
        int cx = srcSpectrum.cols / 2.;
        int cy =srcSpectrum.rows / 2.; 

        cv::Mat dstSpectrum, complexOutput;
        cv::Mat planes[] = {cv::Mat_<float>(srcSpectrum), cv::Mat::zeros(srcSpectrum.size(), CV_32F)};
        cv::Mat planes_[] = {cv::Mat_<float>(srcSpectrum), cv::Mat::zeros(srcSpectrum.size(), CV_32F)};

        cv::Mat q0(srcSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(srcSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(srcSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(srcSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);

        cv::split(complexSpectrum.transformMat, planes);
        cv::split(srcSpectrum, planes_);

        cv::merge(planes, 2, complexOutput);
        cv::Mat dsttmp, dst1;
        cv::multiply(complexOutput, srcSpectrum, dstSpectrum);
        cv::split(dstSpectrum, planes);
      
      /*
        cv::magnitude(planes[0], planes[1], dst1);
        cv::Mat dst1q0(dst1, cv::Rect(0, 0, cx, cy));
        cv::Mat dst1q1(dst1, cv::Rect(cx, 0, cx, cy));
        cv::Mat dst1q2(dst1, cv::Rect(0, cy, cx, cy));
        cv::Mat dst1q3(dst1, cv::Rect(cx, cy, cx, cy));
        cv::Mat dst1tmp;
        dst1q0.copyTo(dst1tmp); dst1q3.copyTo(dst1q0); dst1tmp.copyTo(dst1q3);
        dst1q1.copyTo(dst1tmp); dst1q2.copyTo(dst1q1); dst1tmp.copyTo(dst1q2);

        
        dst1 += cv::Scalar(1);
        log(dst1, dst1);
        cv::normalize(dst1, dst1, 0, 1, cv::NORM_MINMAX);
        std::cout<<"srcSpectrum type: "<<dst1.type()<<std::endl;
        std::cout<<"dst rows: "<<dst1.rows<<"dst cols: "<<dst1.cols<<std::endl;
        return dst1;
        */
        
        cv::Mat mag, ph;
        cv::magnitude(planes[0], planes[1], mag);
        cv::phase(planes[0], planes[1], ph);
        
        cv::Mat dst, complexIDFT, iDFToutput;
        cv::idft(dstSpectrum, complexIDFT);
        cv::split(complexIDFT, planes);
        cv::magnitude(planes[0], planes[1], dst);
        cv::normalize(dst, dst, 1, 0, cv::NORM_MINMAX);
               
        std::cout<<"dst type: "<< dst.type()<<std::endl;
        return dst;
    };


    Corners getCornerFeatures(cv::Mat src, int maxCorners, double qualityLevel, double minDistance, cv::InputArray mask, int blockSize, bool useHarrisDetector, double k){
        Corners corners;
        cv::goodFeaturesToTrack(src, corners, maxCorners, qualityLevel, minDistance, mask, blockSize, useHarrisDetector, k);
        return corners;
    };

    Corners getCornerSubPix(cv::Mat src, Corners corners, cv::Size winSize, cv::Size zeroZone, cv::TermCriteria criteria){
        cv::cornerSubPix(src, corners, winSize, zeroZone, criteria);
        return corners;
    };

    cv::Mat getRectROI(cv::Mat src, Corner LeftUP, double width, double height){
        cv::Rect2f SearchArea(LeftUP.x-width/1., LeftUP.y-height/1., width*2., height*2.);

        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        cv::Mat dst=src(cv::Rect(SearchArea.x, SearchArea.y, SearchArea.width, SearchArea.height));

        return dst;
    };

    cv::Mat getRectROI(cv::Mat src, cv::Rect SearchArea){

        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        cv::Mat dst=src(cv::Rect(SearchArea.x, SearchArea.y, SearchArea.width, SearchArea.height));

        return dst;
    };

    cv::Rect RectROI(cv::Mat src, cv::Rect SearchArea){
        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        return SearchArea;
    };

    cv::Mat DilateErode(cv::Mat src, std::vector<int> Mat_erode, std::vector<int> Mat_dilate){
        cv::Mat image_dilate, image_erode;
        cv::Mat element_erode, element_dilate;
        cv::Mat image = {};

        for (int i = 0; i < Mat_dilate.size(); i++){
            if (i == 0){
                image = src;
            }
            if (i != 0){
                image = image_dilate;
            }
            if (true){
                element_erode = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(Mat_erode.at(i),Mat_erode.at(i)));
                cv::erode(image, image_erode, element_erode);
                element_dilate = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(Mat_dilate.at(i),Mat_dilate.at(i)));
                cv::dilate(image_erode, image_dilate, element_dilate);
            }

            image = image_dilate;
            if (i == Mat_dilate.size()-1){
                return image; 
            }
        }
    };


};

namespace ImageShow {
    void showImage(cv::Mat image, double scale, const std::string ImageName, bool TrackBar){
        std::cout<<"image type: "<<image.type()<<std::endl;
        cv::Mat dst;
        if (image.type() < 8 || image.type() >= 16){
            dst = image;
            
        }

        else if (image.type() >= 8 && image.type() < 16){
            cv::Mat planes[] = {cv::Mat_<float>(image), cv::Mat::zeros(image.size(), CV_32F)};
            cv::split(image, planes);
            cv::magnitude(planes[0], planes[1], dst);
            dst += cv::Scalar(1);
            cv::log(dst, dst);
            cv::normalize(dst,dst,1,0,cv::NORM_MINMAX);
        }

        std::cout<<"dst type: "<<dst.type() << ", channels: " << dst.channels() << " , rows: "<< dst.rows << " , cols: "<< dst.cols << std::endl;
        if (!TrackBar){
            cv::resize(dst, dst, cv::Size(dst.cols*scale, dst.rows*scale));
            cv::imshow(ImageName, dst);
        }

    };

};

namespace ManualOperation{

};