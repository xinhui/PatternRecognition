#include "Filter.h"
namespace Filter{
    cv::Mat GaussianBlur(cv::Mat src, cv::Size ksize, double sigmaX, double sigmaY, int borderType){
        cv::Mat dst;
        cv::GaussianBlur(src, dst, ksize, sigmaX, sigmaY, borderType);
        return dst;
    };


    cv::Mat GaussianBandFreqFilter(cv::Mat srcSpectrum, double sigmaX, double sigmaY, double centX, double centY){
        std::cout<<"Gaussian Band freq filter"<<std::endl;
        int width = srcSpectrum.cols;
        int height =srcSpectrum.rows;

        cv::Mat Gauss_band = cv::Mat::zeros(height, width, CV_32FC2);
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                double disX = (std::pow((j-centX),2));
                double disY = (std::pow((i-centY),2));
                Gauss_band.at<float>(i,2*j) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
                Gauss_band.at<float>(i,2*j+1) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
            }
        }
    
        return Gauss_band;

    };


    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, double X, double Y, double width, double height){
        std::cout<<"Band freq filter"<<std::endl;
        cv::Mat band = cv::Mat::zeros(srcSpectrum.size(), CV_32FC2);
        if ( X + width >= srcSpectrum.cols){
            width = srcSpectrum.cols - X;
        } 
        else if ( Y + height >= srcSpectrum.rows){
            height = srcSpectrum.rows - Y;
        }

        for (int i = 0; i < srcSpectrum.rows; i++){
            for (int j = 0; j < srcSpectrum.cols; j++){
                if ( X < j && j < X + width && Y < i && i < Y + height){
                    band.at<float>(i,2*j) = 0;
                    band.at<float>(i,2*j+1) = 0;
                }
                else{
                    band.at<float>(i,2*j) = 1;
                    band.at<float>(i,2*j+1) = 1;
                }
            }
        }
    
        return band;
    }

}