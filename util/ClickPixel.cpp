#include <iostream>
#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include "ImageProcess.h"


cv::Mat *img, *img_sec;
cv::Point2f corner_;
cv::Mat tmp, temp, dst, cic;   
int level = 0;

void onMouse(int event, int x, int y, int flags, void* param){

	static cv::Point pre_pt = cv::Point(-1, -1);
	static cv::Point cur_pt = cv::Point(-1, -1);
	static cv::Point cir_pt = cv::Point(-1, -1);

	cv::Mat *im = reinterpret_cast<cv::Mat *>(param);
	switch (event){
        case 0:{
            if (level == 0){
                if(event == 0 && flags == 2){
		            (*im).copyTo(tmp);
		            cur_pt = cv::Point(x, y);
		            cv::rectangle(tmp, pre_pt, cur_pt, cv::Scalar(255, 255, 0, 0), 1, 8, 0);
		            std::cout << cur_pt << std::endl;
		            cv::imshow("original image", tmp);
                }
            }
        }
            break;
	    case 1:{
            if (level == 0){
		        (*im).copyTo(cic);
                corner_.x = x; corner_.y = y;
                cv::circle(cic, corner_, 1, cv::Scalar(0, 0, 255), 2, 8, 0);
	            cv::imshow("original image", cic);
		        std::cout << "at original image: " << x << "," << y << ")value is:" << static_cast<int>(im->at<uchar>(cv::Point(x, y))) << std::endl;
            }
            if (level == 1){
		        (*im).copyTo(cic);
                corner_.x = x; corner_.y = y;
                cv::circle(cic, corner_, 1, cv::Scalar(0, 0, 255), 2, 8, 0);
	            cv::imshow("cut image", cic);
		        std::cout << "at cut image: " << pre_pt.x+x << "," << pre_pt.y+y << ")value is:" << static_cast<int>(im->at<uchar>(cv::Point(x, y))) << std::endl;
            }
        }   
		    break;  
        case 2:{
            if (level == 0){
		        pre_pt = cv::Point(x, y);
		        std::cout << pre_pt << std::endl;
            }
        }
            break;
        case 5:{
            if (level == 0){
		        (*im).copyTo(temp);
		        cur_pt = cv::Point(x, y);
		        std::cout << cur_pt << std::endl;
		        int width = abs(pre_pt.x - cur_pt.x);
		        int height = abs(pre_pt.y - cur_pt.y);
                if (width*height != 0){
                    level = 1;
		            dst = temp(cv::Rect(std::min(cur_pt.x, pre_pt.x), std::min(cur_pt.y, pre_pt.y), width, height));
	                cv::namedWindow("cut image", cv::WINDOW_AUTOSIZE);
                    img_sec = &dst;
	                cv::setMouseCallback("cut image", onMouse, reinterpret_cast<void*> (img_sec));
		            cv::imshow("cut image", *img_sec); 
                }
            }
        }
            break;
	}
};


int main(){

	cv::Mat src = cv::imread("/Users/xinhui/Desktop/PatternRecognition/images/Hybrid/Hybrid_20230616_11.bmp");
	if (src.empty()){
		std::cout << "File is empty!" << std::endl;
	}
    else{
	    img = &src;
        level = 0;
	    cv::namedWindow("original image", cv::WINDOW_AUTOSIZE);
	    cv::setMouseCallback("original image", onMouse, reinterpret_cast<void*> (img));
	    cv::imshow("original image", *img);

	    cv::waitKey(0);
    }
	return 0;

}

