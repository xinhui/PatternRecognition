#include "ImageProcess.h"
#include "ImageLoading.h"
#include "Filter.h"

int thres_value = 180;
int thres_Max = 255;
int cornerNum = 9;
int maxCorner = 13;
int CornerDistance = 200;
//resize image scale
double scale = 0.5;

double search_size = 60;


int main(){
    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images/"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Glass;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "05", type);

    for (int i = 0; i < imageFiles.size(); i++){

		cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));
        cv::Mat srcImage_8U;
        cv::cvtColor(srcImage, srcImage_8U, cv::COLOR_RGB2GRAY);

        cv::Mat binaryImage = ImageProcess::getBinaryImage(srcImage, thres_value, thres_Max, cv::THRESH_BINARY);
        ImageShow::showImage(binaryImage, scale, "binaryImage");
        
        cv::Mat adaptiveBinaryImage;
        cv::adaptiveThreshold(srcImage_8U, adaptiveBinaryImage, 180, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 3, 0);
        //ImageShow::showImage(adaptiveBinaryImage, scale, "adaptiveBinaryImage");

        cv::Mat Spectrum = ImageProcess::getSpectrumDFT(binaryImage);
        //mageShow::showImage(Spectrum, scale, "Spectrum");

        ComplexSpectrum complexSpectrum = ImageProcess::getComplexSpectrum(binaryImage);

        cv::Mat blurSpectrum1 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 620);
        cv::Mat blurSpectrum2 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 840);
        cv::Mat blurSpectrum3 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 930);
        cv::Mat blurSpectrum4 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 1210);
        cv::Mat blurSpectrum5 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 1300);
        cv::Mat blurSpectrum6 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 10, 10, 1216, 1420);
        
        cv::Mat blurSpectrum_rectband = Filter::BandFreqFilter(complexSpectrum.transformMat, 1210, 0, 12, 1010);
        cv::Mat blurSpectrum_rectband2 = Filter::BandFreqFilter(complexSpectrum.transformMat, 1210, 1030, 12, 2000);
        cv::Mat blurSpectrum = blurSpectrum1.mul(blurSpectrum2).mul(blurSpectrum3).mul(blurSpectrum4).mul(blurSpectrum5).mul(blurSpectrum6).mul(blurSpectrum_rectband.mul(blurSpectrum_rectband2));
        //cv::Mat blurSpectrum = blurSpectrum_rectband + blurSpectrum_rectband2 + blurSpectrum1+ (blurSpectrum2)+ (blurSpectrum3) + (blurSpectrum4) + (blurSpectrum5) + (blurSpectrum6);
        cv::normalize(blurSpectrum, blurSpectrum, 0, 1, cv::NORM_MINMAX);
        ImageShow::showImage(blurSpectrum, scale, "blurSpectrum");
        std::cout<<blurSpectrum.type()<<std::endl;

        cv::Mat bluriDFTImage = ImageProcess::getSpectrumiDFT(blurSpectrum, complexSpectrum, 5);
        ImageShow::showImage(bluriDFTImage, scale, "bluriDFTImage");
        //std::cout<<bluriDFTImage.type()<<std::endl;

        cv::Mat Spectrum2 = ImageProcess::getSpectrumDFT(bluriDFTImage);
        ImageShow::showImage(Spectrum2, scale, "Spectrum2");

        
        
    }

// end program
    cv::waitKey(0);
    return 0;
    
}