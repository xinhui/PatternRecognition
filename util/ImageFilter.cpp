// CornerRecognition for test

#include<stdio.h>
#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include<cmath>

int thres_value = 130;
int thres_Max = 255;
// main
cv::Mat srcImage, imageResize, imageGrey;
// binary Grey 
void BinaryGrey(int, void*);
cv::Mat binaryGreyImage;
// spectrum mat
void SpectrumDFT(int, void*);
cv::Mat imageSpectrumBinaryGreyScale, imageSpectrum;
// sub-pixel corner recognition
void SubpxCornerRecog(int, void*);
int cornerNum = 9;
int maxCorner = 9;
//resize image scale
double scale = 1;


int main()
{
    srcImage = cv::imread("./images/CaliPic/Cali_20230531_01.bmp");
    //srcImage = cv::imread("./images/ChessBoard/Cali_20220909_4.bmp");

    if( srcImage.empty()){
        printf("Could not load image..\n");
        return 0;
    }

    cv::Mat imageSobel;
    cv::cvtColor(srcImage, imageGrey, cv::COLOR_RGB2GRAY);

// Binary Grey scale 
    cv::namedWindow("binaryGreyImage");
    cv::createTrackbar("Threshold", "binaryGreyImage", &thres_value, thres_Max, BinaryGrey);
    BinaryGrey(0, 0);


// FFT-> frequency spectrum of the srcGreyImage
    cv::namedWindow("srcGreySpectrum");
    SpectrumDFT(0, 0);

// FFT -> frequency spectrum scan
    //cv::namedWindow("Spectrum");
    //cv::createTrackbar("Threshold", "Spectrum", &thres_value, thres_Max, SpectrumDFT);
    //SpectrumDFT(0, 0);

// end program
    cv::waitKey(0);
    return 0;
}

void BinaryGrey(int, void *){
    cv::threshold(imageGrey, binaryGreyImage, thres_value, 255, cv::THRESH_BINARY); 
    cv::resize(binaryGreyImage, binaryGreyImage, cv::Size(2432*scale, 2040*scale)); 
    cv::imshow("binaryGreyImage", binaryGreyImage);

}

void SpectrumDFT(int, void*){
    cv::threshold(imageGrey, imageSpectrumBinaryGreyScale, thres_value, thres_Max, cv::THRESH_BINARY); 
    cv::Mat transformMat;
    cv::Mat planes[] = {cv::Mat_<float>(imageSpectrumBinaryGreyScale), cv::Mat::zeros(imageSpectrumBinaryGreyScale.size(), CV_32F)};
    cv::merge(planes, 2, transformMat);
    cv::dft(transformMat, transformMat);
    cv::split(transformMat, planes);
    cv::magnitude(planes[0], planes[1], imageSpectrum);

    imageSpectrum += cv::Scalar(1);
    log(imageSpectrum, imageSpectrum);
    cv::normalize(imageSpectrum, imageSpectrum, 0, 1, cv::NORM_MINMAX);
    imageSpectrum = imageSpectrum(cv::Rect(0, 0, imageSpectrum.cols & -2, imageSpectrum.rows & -2));

    int cx = imageSpectrum.cols / 2;
    int cy =imageSpectrum.rows / 2; 
    cv::Mat q0(imageSpectrum, cv::Rect(0, 0, cx, cy));
    cv::Mat q1(imageSpectrum, cv::Rect(cx, 0, cx, cy));
    cv::Mat q2(imageSpectrum, cv::Rect(0, cy, cx, cy));
    cv::Mat q3(imageSpectrum, cv::Rect(cx, cy, cx, cy));

    cv::Mat tmp;
    q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
    q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);

    cv::resize(imageSpectrum, imageSpectrum, cv::Size(2432*scale, 2040*scale));
    cv::imshow("Spectrum", imageSpectrum);
}

