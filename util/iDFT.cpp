
#include <iostream>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/calib3d.hpp>

#include "ImageProcess.h"

#define IMG_PATH "/Users/xinhui/Desktop/PatternRecognition/images/lena.jpg"

using namespace std;
using namespace cv;

int main()
{
    Mat inputImg = imread(IMG_PATH, IMREAD_GRAYSCALE);

    if(inputImg.empty())
    {
        cout << "图片没读到，傻逼！" << endl;
        return -1;
    }

    //得到DFT的最佳尺寸（2的指数），以加速计算
    Mat paddedImg;
    int m = getOptimalDFTSize(inputImg.rows);
    int n = getOptimalDFTSize(inputImg.cols);

    cout << "图片原始尺寸为：" << inputImg.cols << "*" << inputImg.rows <<endl;
    cout << "DFT最佳尺寸为：" << n << "*" << m <<endl;

    //填充图像的下端和右端
    //copyMakeBorder(inputImg, paddedImg, 0, m - inputImg.rows,
                   //0, n - inputImg.cols, BORDER_CONSTANT, Scalar::all(0));
    paddedImg = inputImg;
    //将填充的图像组成一个复数的二维数组（两个通道的Mat），用于DFT
    Mat matArray[] = {Mat_<float>(paddedImg), Mat::zeros(paddedImg.size(), CV_32F)};
    Mat complexInput, complexOutput;
    merge(matArray, 2, complexInput);

    dft(complexInput, complexOutput);

    //计算幅度谱（傅里叶谱）
    split(complexOutput, matArray);
    Mat magImg;
    magnitude(matArray[0], matArray[1], magImg);

    //转换到对数坐标
    magImg += Scalar::all(1);
    log(magImg, magImg);

    //将频谱图像裁剪成偶数并将低频部分放到图像中心
    int width = (magImg.cols / 2)*2;
    int height = (magImg.cols / 2)*2;
    magImg = magImg(Rect(0, 0, width, height));

    int colToCut = magImg.cols/2;
    int rowToCut = magImg.rows/2;

    //获取图像，分别为左上右上左下右下
    //注意这种方式得到的是magImg的ROI的引用
    //对下面四幅图像进行修改就是直接对magImg进行了修改
    Mat topLeftImg(magImg, Rect(0, 0, colToCut, rowToCut));
    Mat topRightImg(magImg, Rect(colToCut, 0, colToCut, rowToCut));
    Mat bottomLeftImg(magImg, Rect(0, rowToCut, colToCut, rowToCut));
    Mat bottomRightImg(magImg, Rect(colToCut, rowToCut, colToCut, rowToCut));

    //第二象限和第四象限进行交换
    Mat tmpImg;
    topLeftImg.copyTo(tmpImg);
    bottomRightImg.copyTo(topLeftImg);
    tmpImg.copyTo(bottomRightImg);

    //第一象限和第三象限进行交换
    bottomLeftImg.copyTo(tmpImg);
    topRightImg.copyTo(bottomLeftImg);
    tmpImg.copyTo(topRightImg);

    //归一化图像
    normalize(magImg, magImg, 0, 1, NORM_MINMAX);

    //傅里叶反变换
    Mat complexIDFT, IDFTImg;
    idft(complexOutput, complexIDFT);
    split(complexIDFT, matArray);
    magnitude(matArray[0], matArray[1], IDFTImg);
    normalize(IDFTImg, IDFTImg, 0, 1, NORM_MINMAX);

    //imshow("输入图像", inputImg);
    //imshow("频谱图像", magImg);
    //imshow("反变换图像", IDFTImg);

    /***********************频率域滤波部分*************************/
    //高斯低通滤波函数（中间高两边低）
    Mat gaussianBlur(paddedImg.size(),CV_32FC2);
    //高斯高通滤波函数（中间低两边高）
    Mat gaussianSharpen(paddedImg.size(),CV_32FC2);
    double D0=2*10*10*10;
    double sigmaX = 10, sigmaY = 10;
    double centX = paddedImg.cols/2;
    double centY = paddedImg.rows/2;
    for(int i=0;i<paddedImg.rows;i++)
    {
        float*p=gaussianBlur.ptr<float>(i);
        float*q=gaussianSharpen.ptr<float>(i);
        for(int j=0;j<paddedImg.cols;j++)
        {
            double disX = (std::pow((j-centX),2));
            double disY = (std::pow((i-centY),2));
            gaussianBlur.at<float>(i,2*j) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
            gaussianBlur.at<float>(i,2*j+1) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
        }
    }
    //gaussianBlur = ImageProcess::GaussianBandFreqFilter(paddedImg, 10, 10, 200, 200);
    ImageShow::showImage(gaussianBlur, 0.5, "gaussianBlur");
    //将实部和虚部按照频谱图的方式换位
    //低频在图像中心，用于滤波
    split(complexOutput, matArray);

    //matArray[0]表示DFT的实部
    Mat q1(matArray[0], Rect(0, 0, colToCut, rowToCut));
    Mat q2(matArray[0], Rect(colToCut, 0, colToCut, rowToCut));
    Mat q3(matArray[0], Rect(0, rowToCut, colToCut, rowToCut));
    Mat q4(matArray[0], Rect(colToCut, rowToCut, colToCut, rowToCut));

    //第二象限和第四象限进行交换
    q1.copyTo(tmpImg);
    q4.copyTo(q1);
    tmpImg.copyTo(q4);

    //第一象限和第三象限进行交换
    q2.copyTo(tmpImg);
    q3.copyTo(q2);
    tmpImg.copyTo(q3);

    //matArray[1]表示DFT的虚部
    Mat qq1(matArray[1], Rect(0, 0, colToCut, rowToCut));
    Mat qq2(matArray[1], Rect(colToCut, 0, colToCut, rowToCut));
    Mat qq3(matArray[1], Rect(0, rowToCut, colToCut, rowToCut));
    Mat qq4(matArray[1], Rect(colToCut, rowToCut, colToCut, rowToCut));

    //第二象限和第四象限进行交换
    qq1.copyTo(tmpImg);
    qq4.copyTo(qq1);
    tmpImg.copyTo(qq4);

    //第一象限和第三象限进行交换
    qq2.copyTo(tmpImg);
    qq3.copyTo(qq2);
    tmpImg.copyTo(qq3);

    merge(matArray, 2, complexOutput);

    //滤波器和DFT结果相乘（矩阵内积）
    //multiply(complexOutput,gaussianBlur,gaussianBlur);
    //multiply(complexOutput,gaussianSharpen,gaussianSharpen);

    //计算频谱
    split(gaussianBlur,matArray);
    magnitude(matArray[0],matArray[1],magImg);
    magImg+=Scalar::all(1);
    log(magImg,magImg);
    normalize(magImg,magImg,1,0,NORM_MINMAX);
    //imshow("高斯低通滤波频谱",magImg);

    split(gaussianSharpen,matArray);
    magnitude(matArray[0],matArray[1],magImg);
    magImg+=Scalar::all(1);
    log(magImg,magImg);
    normalize(magImg,magImg,1,0,NORM_MINMAX);
    //imshow("高斯高通滤波频谱",magImg);

    //IDFT得到滤波结果
    Mat gaussianBlurImg;
    idft(gaussianBlur, complexIDFT);
    split(complexIDFT, matArray);
    magnitude(matArray[0], matArray[1], gaussianBlurImg);
    normalize(gaussianBlurImg, gaussianBlurImg, 0, 1, NORM_MINMAX);

    Mat gaussianSharpenImg;
    idft(gaussianSharpen, complexIDFT);
    split(complexIDFT, matArray);
    magnitude(matArray[0], matArray[1], gaussianSharpenImg);
    normalize(gaussianSharpenImg, gaussianSharpenImg, 0, 1, NORM_MINMAX);

    //imshow("高斯低通滤波", gaussianBlurImg);
    //imshow("高斯高通滤波", gaussianSharpenImg);


    waitKey(0);

    return 0;
}
