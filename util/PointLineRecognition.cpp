#include "ImageLoading.h"
#include "ImageProcess.h"
#include "Filter.h"

using namespace cv;
using namespace std;

double scale = 1;

int main(){
    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images/"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Hybrid;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "11", type);

    for (int i = 0; i < imageFiles.size(); i++){

	    cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));
        cv::Mat srcImage_Gray, edge, dst;
        cv::cvtColor(srcImage, srcImage_Gray, cv::COLOR_RGB2GRAY);
        cv::Canny(srcImage_Gray, edge, 80, 180, 3, false);         // ASIC: 100, 220,  Hybrid: 100, 180
        cv::threshold(edge, edge, 100, 255, THRESH_BINARY);

        cv::Mat ROI, cross, circle;
        int x = 1502;
        int y = 1437;
        for (int i = x; i < x+1; i++){      // 1751, 740
            for (int j = y; j < y+1; j++){
                double meanGrayValue_cross_m = 0, meanGrayValue_cross_n = 0;
                double meanGrayValue_circle = 0;
                cv::Rect2f SearchArea(i-4, j-4, 9, 9);
                SearchArea = ImageProcess::RectROI(edge, SearchArea);
                ROI = ImageProcess::getRectROI(edge, SearchArea);
                for (int m = 0; m < ROI.rows; m++){
                    uchar* data = ROI.ptr<uchar>(m);
                    for (int n = 0; n < ROI.cols; n++){
                        int intensity = data[n];
                        if (m == 4){
                            if ((m+1)*(n+1)!=25){
                                meanGrayValue_cross_m = meanGrayValue_cross_m + (double)intensity/8.;
                            }
                        }
                        if (n == 4){
                            if ((m+1)*(n+1)!=25){
                                meanGrayValue_cross_n = meanGrayValue_cross_n + (double)intensity/8.;
                            }
                            //std::cout<<m<<", "<<n<< ", "<< intensity << std::endl;
                        }
                        if((m+1)*(n+1)!=25){
                            meanGrayValue_circle = meanGrayValue_circle + (double)intensity/80.;
                        }
                    } 
                }
                meanGrayValue_cross_m = meanGrayValue_cross_m/255.;
                meanGrayValue_cross_n = meanGrayValue_cross_n/255.;
                meanGrayValue_circle = meanGrayValue_circle/255.;
            ImageShow::showImage(ROI, 40, "ROI");
                    std::cout<< meanGrayValue_cross_m<<", "<< meanGrayValue_cross_n<<", " << meanGrayValue_circle << std::endl;
                if ( ((meanGrayValue_cross_m >= (1-2./8.)) &&  (meanGrayValue_cross_m <= (1)))
                || ((meanGrayValue_cross_n >= (1-2./8.)) &&  (meanGrayValue_cross_n <= (1)))){
                    if (((meanGrayValue_circle < (0.1-6./80.)) &&  (meanGrayValue_circle < (0.1+6./80.)))
                    ||  ((meanGrayValue_circle < (0.3-6./80.)) &&  (meanGrayValue_circle < (0.3+6./80.)))
                    ||  ((meanGrayValue_circle < (0.5-6./80.)) &&  (meanGrayValue_circle < (0.5+6./80.)))){
                        edge.at<uchar>(j,i) = 255;
                        std::cout<<"----- edge point! -----"<<std::endl;
                    }
                    else{
                        edge.at<uchar>(j,i) = 0;
                        std::cout<<"inner point!"<<std::endl;
                    }
                }
                else{
                    edge.at<uchar>(j,i) = 0;
                    std::cout<<"inner point!"<<std::endl;
                }
            }

        }

        cv::Rect2f SearchArea(x-4, y-4, 9, 9);
        ROI = ImageProcess::getRectROI(edge, SearchArea);
        ImageShow::showImage(ROI, 40, "ROI1");

/*
        std::vector<Vec4i> linesP1, linesP2;
        cv::HoughLinesP(edge, linesP1, 1, CV_PI / 180, 50, 300, 100);  
        cv::HoughLinesP(edge, linesP2, 1, CV_PI / 180, 150, 300, 100); 

        cv::Mat img1;
        srcImage_Gray.copyTo(img1);
        for (size_t i = 0; i < linesP1.size(); i++){
            cv::line(img1, cv::Point(linesP1[i][0], linesP1[i][1]),
                cv::Point(linesP1[i][2], linesP1[i][3]), cv::Scalar(255), 3);
        }

        cv::Mat img2;
        srcImage_Gray.copyTo(img2);
        for (size_t i = 0; i < linesP2.size(); i++){
            cv::line(img2, cv::Point(linesP2[i][0], linesP2[i][1]),
                cv::Point(linesP2[i][2], linesP2[i][3]), cv::Scalar(255), 3);
        }

        //ImageShow::showImage(img1, scale, "img1");
        //ImageShow::showImage(img2, scale, "img2");
*/

    }


   
	if (imageFiles.size() != 0){
        waitKey(0);
	}
    return 0;
}