// Focus Evaluation

#include "ImageLoading.h"
#include "ImageProcess.h"


int main(){

    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Focus;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, array, type);

    for (int i = 0; i < imageFiles.size(); i++){

		cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));

        double meanGrayValue = ImageProcess::getImageMeanGradValue(srcImage, CV_16U, 1, 1);
	    double meanStdValue = ImageProcess::getImageMeanStdValue(srcImage);

        std::cout<< "Pic: " << std::to_string(i+1) <<", Articulation: " << meanGrayValue << ", Std Dev: "<< meanStdValue
                 << ", Combined: " <<  meanGrayValue*meanStdValue <<std::endl;
	}

    return 0;
}

