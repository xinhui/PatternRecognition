#include "ImageProcess.h"
#include "ImageLoading.h"

int thres_value = 180;
int thres_Max = 255;
int cornerNum = 9;
int maxCorner = 13;
int CornerDistance = 200;
//resize image scale
double scale = 0.5;

double search_size = 60;


int main(){
    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Glass;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "05", type);

    for (int i = 0; i < imageFiles.size(); i++){

		cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));
        cv::Mat srcImage_8U;
        cv::cvtColor(srcImage, srcImage_8U, cv::COLOR_RGB2GRAY);

        cv::Mat binaryImage = ImageProcess::getBinaryImage(srcImage, thres_value, thres_Max, cv::THRESH_BINARY);
        //ImageShow::showImage(binaryImage, scale, "binaryImage");
        
        srcImage_8U.convertTo(srcImage_8U, CV_8UC1);
        cv::Mat adaptiveBinaryImage;
        cv::adaptiveThreshold(srcImage_8U, adaptiveBinaryImage, 180, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 3, 0);
        //ImageShow::showImage(adaptiveBinaryImage, scale, "adaptiveBinaryImage");

        cv::Mat Spectrum = ImageProcess::getSpectrumDFT(binaryImage);
        //ImageShow::showImage(Spectrum, scale, "Spectrum");

        cv::Mat blurSpectrum = ImageProcess::GaussianFreqFilter(binaryImage, 300, 5, 5);
        //ImageShow::showImage(blurSpectrum, scale, "blurSpectrum");

        cv::Mat plane[]={cv::Mat_<float>(srcImage_8U),cv::Mat::zeros(srcImage_8U.size(),CV_32F)};//创建通道
	    cv::Mat complexIm;
	    cv::merge(plane,2,complexIm);//合并通道
	    cv::dft(complexIm,complexIm);//进行傅立叶变换，结果保存在自身
	    cv::split(complexIm,plane);//分离通道
        cv::Mat logImage;
        cv::Mat mag;
        cv::Mat mag_origin;
        double max, min;

        cv::Mat ph, mag;
	    phase(planes[0], planes[1], ph);
	    magnitude(planes[0], planes[1], mag);     
        mag_origin = plane[0];
	    logImage = plane[0] + cv::Scalar::all(1);//傅立叶变换后的图片不好分析，进行对数处理，结果比较好看
	    cv::log(plane[0],logImage);
	    cv::magnitude(plane[0],plane[1],mag);
        mag += cv::Scalar::all(1);
        cv::log(mag, mag);

	    cv::magnitude(plane[0],plane[1],plane[0]);

        cv::minMaxLoc(mag, &min, &max);
	    cv::normalize(logImage,logImage,0,1,cv::NORM_MINMAX);
	    //cv::normalize(mag,mag,0,1,cv::NORM_MINMAX);

        int cx=srcImage_8U.cols/2;int cy=srcImage_8U.rows/2;//一下的操作是移动图像，左上与右下交换位置，右上与左下交换位置
	    cv::Mat temp;
	    cv::Mat part1(logImage,cv::Rect(0,0,cx,cy));
	    cv::Mat part2(logImage,cv::Rect(cx,0,cx,cy));
	    cv::Mat part3(logImage,cv::Rect(0,cy,cx,cy));
	    cv::Mat part4(logImage,cv::Rect(cx,cy,cx,cy));
	
 
	    part1.copyTo(temp);
	    part4.copyTo(part1);
	    temp.copyTo(part4);
	
	    part2.copyTo(temp);
	    part3.copyTo(part2);
	    temp.copyTo(part3);


	    cv::Mat mag_temp;
	    cv::Mat mag_part1(mag,cv::Rect(0,0,cx,cy));
	    cv::Mat mag_part2(mag,cv::Rect(cx,0,cx,cy));
	    cv::Mat mag_part3(mag,cv::Rect(0,cy,cx,cy));
	    cv::Mat mag_part4(mag,cv::Rect(cx,cy,cx,cy));
	
 
	    mag_part1.copyTo(mag_temp);
	    mag_part4.copyTo(mag_part1);
	    mag_temp.copyTo(mag_part4);
	
	    mag_part2.copyTo(mag_temp);
	    mag_part3.copyTo(mag_part2);
	    mag_temp.copyTo(mag_part3);
        
        //ImageShow::showImage(logImage, scale, "x");
        //ImageShow::showImage(mag, scale, "mag");
        //ImageShow::showImage(plane[1], scale, "y");


        cv::Mat _complexim;

        //cv::normalize(mag, mag, min, max, cv::NORM_MINMAX);
        cv::exp(mag, mag);
        std::cout<<mag.type()<<std::endl;
        mag = mag - cv::Scalar(1);
        ComplexSpectrum iDFTSpectrum;
        cv::Mat iDFToutput;
        cv::polarToCart(mag_origin, plane[1], iDFTSpectrum.magnitude, iDFTSpectrum.phase); 
        cv::Mat planes[] = { iDFTSpectrum.magnitude, iDFTSpectrum.phase };
        cv::merge(planes, 2, iDFToutput);
	    cv::dft(iDFToutput, iDFToutput, cv::DFT_INVERSE);
	    cv::Mat iDft[]={cv::Mat::zeros(plane[0].size(),CV_32F),cv::Mat::zeros(plane[0].size(),CV_32F)};
	    cv::split(iDFToutput,iDft);//结果貌似也是复数
	    cv::magnitude(iDft[0],iDft[1],iDft[0]);//分离通道，主要获取0通道
	    cv::normalize(iDft[0],iDft[0],0,1,cv::NORM_MINMAX);//归一化处理，float类型的显示范围为0-1,大于1为白色，小于0为黑色
        
	    cv::Mat image_B = iDft[0](cv::Rect(0, 0, mag.cols & -2, mag.rows & -2));
        image_B.convertTo(image_B, CV_8U);
        ImageShow::showImage(iDft[0], scale, "image_B");
        
        /*
	    complexIm.copyTo(_complexim);//把变换结果复制一份，进行逆变换，也就是恢复原图
	    cv::Mat iDft[]={cv::Mat::zeros(plane[0].size(),CV_32F),cv::Mat::zeros(plane[0].size(),CV_32F)};//创建两个通道，类型为float，大小为填充后的尺寸
	    cv::idft(_complexim,_complexim);//傅立叶逆变换
	    cv::split(_complexim,iDft);//结果貌似也是复数
	    cv::magnitude(iDft[0],iDft[1],iDft[0]);//分离通道，主要获取0通道
	    cv::normalize(iDft[0],iDft[0],1,0,cv::NORM_MINMAX);//归一化处理，float类型的显示范围为0-1,大于1为白色，小于0为黑色
        std::cout<<iDft[0].type()<<std::endl;
        ImageShow::showImage(iDft[0], scale, "idft");
    //*******************************************************************
	    plane[0]+=cv::Scalar::all(1);//傅立叶变换后的图片不好分析，进行对数处理，结果比较好看
	    cv::log(plane[0],plane[0]);
	    cv::normalize(plane[0],plane[0],1,0,cv::NORM_MINMAX);
        //ImageShow::showImage(plane[0], scale, "dft");

        //ImageShow::showImage(srcImage, scale, "src");
        
    */

    }

// end program
    cv::waitKey(0);
    return 0;
    
}