#include "ImageProcess.h"
#include "ImageLoading.h"
#include "Filter.h"

int thres_value = 174;
int thres_Max = 255;
int cornerNum = 1;
int maxCorner = 1;
int CornerDistance = 100;
//resize image scale
double scale = 0.5;

double search_size = 70;


int main(){
    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images/"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Glass;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "05", type);

    for (int i = 0; i < imageFiles.size(); i++){

		cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));
        cv::Mat srcImage_8U;
        cv::cvtColor(srcImage, srcImage_8U, cv::COLOR_RGB2GRAY);

        cv::Mat binaryImage = ImageProcess::getBinaryImage(srcImage, thres_value, thres_Max, cv::THRESH_BINARY);
        //ImageShow::showImage(binaryImage, scale, "binaryImage");
        
        cv::Mat adaptiveBinaryImage;
        cv::adaptiveThreshold(srcImage_8U, adaptiveBinaryImage, 180, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 3, 0);
        //ImageShow::showImage(adaptiveBinaryImage, scale, "adaptiveBinaryImage");

        cv::Mat Spectrum = ImageProcess::getSpectrumDFT(binaryImage);
        //mageShow::showImage(Spectrum, scale, "Spectrum");

        ComplexSpectrum complexSpectrum = ImageProcess::getComplexSpectrum(binaryImage);

        cv::Mat blurSpectrum1 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 620);
        cv::Mat blurSpectrum2 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 840);
        cv::Mat blurSpectrum3 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 930);
        cv::Mat blurSpectrum4 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 1210);
        cv::Mat blurSpectrum5 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 1300);
        cv::Mat blurSpectrum6 = Filter::GaussianBandFreqFilter(complexSpectrum.transformMat, 5, 5, 1216, 1420);
       
        cv::Mat blurSpectrum_rectband = Filter::BandFreqFilter(complexSpectrum.transformMat, 1210, 0, 16, 1015);
        cv::Mat blurSpectrum_rectband2 = Filter::BandFreqFilter(complexSpectrum.transformMat, 1210, 1025, 16, 2000);
        cv::Mat blurSpectrum = blurSpectrum1.mul(blurSpectrum2).mul(blurSpectrum3).mul(blurSpectrum4).mul(blurSpectrum5).mul(blurSpectrum6).mul(blurSpectrum_rectband.mul(blurSpectrum_rectband2));
        //cv::Mat blurSpectrum = blurSpectrum_rectband.mul(blurSpectrum_rectband2);
        //cv::normalize(blurSpectrum, blurSpectrum, 0, 1, cv::NORM_MINMAX);
        //ImageShow::showImage(blurSpectrum, scale, "blurSpectrum");
        std::cout<<blurSpectrum.type()<<std::endl;
        for (int i = 0; i < 1 ; i++){
            cv::Mat bluriDFTImage = ImageProcess::getSpectrumiDFT(blurSpectrum, complexSpectrum, 5);
            if (i < 5 -1 ){
                complexSpectrum = ImageProcess::getComplexSpectrum(bluriDFTImage);
            }
        }
        cv::Mat bluriDFTImage = ImageProcess::getSpectrumiDFT(blurSpectrum, complexSpectrum, 5);
        //ImageShow::showImage(bluriDFTImage, scale, "bluriDFTImage");
        //std::cout<<bluriDFTImage.type()<<std::endl;

        //cv::Mat Spectrum2 = ImageProcess::getSpectrumDFT(bluriDFTImage); //!!!!!!!!!!
        //ImageShow::showImage(Spectrum2, scale, "Spectrum2");

        // Finish filtering
        // Now start to recognize the corner...
        //cv::Mat Spectrum2 = ImageProcess::getSpectrumDFT(bluriDFTImage); //!!!!!!!!!!
        //ImageShow::showImage(Spectrum2, scale, "Spectrum2");
        Corners corners_rough, corners_, corners;

        //bluriDFTImage = ImageProcess::getBinaryImage(bluriDFTImage, 0.14, 1, cv::THRESH_BINARY);
        //ImageShow::showImage(bluriDFTImage, scale, "bluriDFTImage1");
        corners_rough = ImageProcess::getCornerFeatures(bluriDFTImage, 2, 0.01, CornerDistance, cv::Mat(), search_size, false, 0.04);
        cv::Mat imageResult;
        imageResult = srcImage.clone();
        std::cout<< " finish prelimiary recognization"<<std::endl;
        for (size_t i = 0; i < corners_rough.size()-(corners_rough.size()-corners_rough.size()); i++){
            cv::Rect2f SearchArea(corners_rough[i].x-search_size/1., corners_rough[i].y-search_size/1., search_size*2., search_size*2.);
            SearchArea = ImageProcess::RectROI(bluriDFTImage, SearchArea);
            cv::Mat binaryRectROI = ImageProcess::getRectROI(bluriDFTImage, SearchArea);
            std::cout<< " finish ROI selection"<<std::endl;

            cv::rectangle(bluriDFTImage, SearchArea, cv::Scalar(0, 0, 255), 2);
            ImageShow::showImage(bluriDFTImage, scale, "ROI");
            
        // Filter
            cv::Mat ROI_binary_Filter = Filter::GaussianBlur(binaryRectROI, cv::Size(3, 3), 1.5);
        // Final iteration selection
            std::vector<int> Mat_erode = {1, 10, 3, 5};
            std::vector<int> Mat_dilate = {5, 8, 6, 4};
            cv::Mat ROI_binary_dilate = ImageProcess::DilateErode(ROI_binary_Filter, Mat_erode, Mat_dilate);
            std::cout<< " finish ROI dilate and erode"<<std::endl;
        // get sub pix cornersw
        
            corners_ = ImageProcess::getCornerFeatures(ROI_binary_dilate, 1, 0.1, 0, cv::Mat(), 10, false, 0.04);
            cv::Size size = cv::Size(1, 1);
            cv::TermCriteria tc = cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 40, 0.0001);
            corners_ = ImageProcess::getCornerSubPix(ROI_binary_dilate, corners_, size, cv::Size(-1, -1), tc);
            std::cout<< " finish ROI sub-pix recognition"<<std::endl;
        
        // draw a cricle on the corner
            cv::circle(ROI_binary_dilate, corners_[0], 1, cv::Scalar(0, 0, 255), 2, 8, 0);
            cv::Point2f Point_Corner(corners_[0].x+SearchArea.x, corners_[0].y+SearchArea.y);
            corners.push_back(Point_Corner);
            cv::circle(imageResult, corners[i], 5, cv::Scalar(0, 0, 255), 2, 8, 0);
            
        }

        //ImageShow::showImage(imageResult, scale, "Sub-px Corner Recognition");
        
        for (size_t t = 0; t < corners.size(); t++){
            std::cout<<t + 1 <<".pixel[x, y] = " << corners[t].x << ", "<<corners[t].y <<std::endl;
        }
        
    }

// end program
    cv::waitKey(0);
    return 0;
    
}