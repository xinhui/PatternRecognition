#include<iostream>
#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>


using namespace cv;
using namespace std;
 
Mat src, tmp, img, dst, cic;         
        
void on_Mouse(int event, int x, int y, int flags, void* param) {
	static Point pre_pt = Point(-1, -1);
       
	static Point cur_pt = Point(-1, -1);

	static Point cir_pt = Point(-1, -1);
	if (event == 1){		// Click left button
		pre_pt = Point(x, y);
		std::cout << pre_pt << endl;
	}
	else if (event == cv::EVENT_MOUSEMOVE && flags == 1){
		src.copyTo(tmp);
		cur_pt = Point(x, y);
		rectangle(tmp, pre_pt, cur_pt, Scalar(255, 255, 0, 0), 1, 8, 0);
		std::cout << cur_pt << endl;
		imshow("src", tmp);
	}
	else if (event == 4){		// release left button
		src.copyTo(img);
		cur_pt = Point(x, y);
		std::cout << cur_pt << std::endl;
		rectangle(img, pre_pt, cur_pt, cv::Scalar(0, 0, 255), 2, 8, 0);
		int width = abs(pre_pt.x - cur_pt.x);
		int height = abs(pre_pt.y - cur_pt.y);
		dst = src(Rect(min(cur_pt.x, pre_pt.x), min(cur_pt.y, pre_pt.y), width, height));
 
		namedWindow("dst", cv::WINDOW_NORMAL);
		imshow("dst", dst);
	}
	else if (event == 2){		// click right button
		src.copyTo(cic);
		cir_pt = Point(x, y);
		std::cout << cic.cols<<", "<<cic.rows << std::endl;
		std::cout << cir_pt << std::endl;
		circle(cic, cir_pt, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
		imshow("src", cic);
	}
}
 
int main()
{
	src = cv::imread("/Users/xinhui/Desktop/PatternRecognition/images/Glass/220926_143557_0000000005.bmp");
	
	namedWindow("src");
	
	setMouseCallback("src", on_Mouse,0);
 
	imshow("src", src);
 
	waitKey(0);
}	
