// ChessboardRecognition for test

#include "ImageLoading.h"
#include "ImageProcess.h"
#include "Filter.h"

int thres_value = 130;
int thres_Max = 255;
int cornerNum = 9;
int maxCorner = 13;
int CornerDistance = 200;
//resize image scale
double scale = 1;

double search_size = 60;


int main(){

    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Chessboard;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "01", type);

    for (int i = 0; i < imageFiles.size(); i++){

		cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));

        cv::Mat binaryImage = ImageProcess::getBinaryImage(srcImage, thres_value, thres_Max, cv::THRESH_BINARY);
        //ImageShow::showImage(binaryImage, scale, "binaryImage");

        cv::Mat Spectrum = ImageProcess::getSpectrumDFT(binaryImage);
        //ImageShow::showImage(Spectrum, scale, "spectrum");
	

        cv::Mat srcGrayImage, binaryGrayImage;
        cv::cvtColor(srcImage, srcGrayImage, cv::COLOR_RGB2GRAY);
        binaryGrayImage = ImageProcess::getBinaryImage(srcGrayImage, thres_value, thres_Max, cv::THRESH_BINARY);

        Corners corners_rough, corners_, corners;
        cv::Mat binaryGrayImage_GaussianFilter = Filter::GaussianBlur(binaryGrayImage, cv::Size(3, 3), 1.5);
        //ImageShow::showImage(binaryGrayImage_GaussianFilter, scale, "binaryGrayImage_GaussianFilter");
        corners_rough = ImageProcess::getCornerFeatures(binaryGrayImage_GaussianFilter, maxCorner, 0.01, CornerDistance, cv::Mat(), search_size, false, 0.04);
        
        cv::Mat imageResult;
        imageResult = srcImage.clone();
        std::cout<< " finish prelimiary recognization"<<std::endl;

        for (size_t i = 0; i < corners_rough.size()-(corners_rough.size()-corners_rough.size()); i++){
            cv::Rect2f SearchArea(corners_rough[i].x-search_size/1., corners_rough[i].y-search_size/1., search_size*2., search_size*2.);
            SearchArea = ImageProcess::RectROI(binaryGrayImage, SearchArea);
            cv::Mat binaryRectROI = ImageProcess::getRectROI(binaryGrayImage, SearchArea);
        
        // Filter
            cv::Mat ROI_binary_Filter = Filter::GaussianBlur(binaryRectROI, cv::Size(3, 3), 1.5);
        // Final iteration selection
            std::vector<int> Mat_erode = {1, 10, 3, 5};
            std::vector<int> Mat_dilate = {5, 8, 6, 4};
            cv::Mat ROI_binary_dilate = ImageProcess::DilateErode(ROI_binary_Filter, Mat_erode, Mat_dilate);
            
        // get sub pix corners
            corners_ = ImageProcess::getCornerFeatures(ROI_binary_dilate, 1, 0.001, 0, cv::Mat(), 3, false, 0.04);
            cv::Size size = cv::Size(1, 1);
            cv::TermCriteria tc = cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 40, 0.0001);
            corners_ = ImageProcess::getCornerSubPix(ROI_binary_dilate, corners_, size, cv::Size(-1, -1), tc);
            
        // draw a cricle on the corner
            cv::circle(ROI_binary_dilate, corners_[0], 1, cv::Scalar(0, 0, 255), 2, 8, 0);
            cv::Point2f Point_Corner(corners_[0].x+SearchArea.x, corners_[0].y+SearchArea.y);
            corners.push_back(Point_Corner);
            cv::circle(imageResult, corners[i], 5, cv::Scalar(0, 0, 255), 2, 8, 0);
            
        }

        ImageShow::showImage(imageResult, scale, "Sub-px Corner Recognition");
        
        for (size_t t = 0; t < corners.size(); t++){
            std::cout<<t + 1 <<".pixel[x, y] = " << corners[t].x << ", "<<corners[t].y <<std::endl;
        }

    }

// end program
    cv::waitKey(0);
    return 0;
}


