#include "ImageLoading.h"
#include "ImageProcess.h"
#include "Filter.h"

using namespace cv;
using namespace std;

double scale = 0.8;

int main(){
    std::string ImageLoc = "/Users/xinhui/Desktop/PatternRecognition/images/"; 
    ImageUtils::Format format = ImageUtils::Format::bmp;
    ImageUtils::Type type = ImageUtils::Type::Hybrid;
    ImageFiles array = {"01", "02", "11", "08", "20"};

    ImageFiles imageFiles = ImageLoading::getImages(ImageLoc, format, "07", type);

    for (int i = 0; i < imageFiles.size(); i++){

	    cv::Mat srcImage = ImageLoading::readImage(imageFiles.at(i));
        cv::Mat srcImage_Gray, edge, dst, edge1, edge2;
        cv::cvtColor(srcImage, srcImage_Gray, cv::COLOR_RGB2GRAY);
        srcImage_Gray = Filter::GaussianBlur(srcImage_Gray, cv::Size(9, 9), 3, 3);
        cv::Canny(srcImage_Gray, edge, 100, 180, 3, false);         // ASIC: 100, 220,  Hybrid: 100, 180
        cv::threshold(edge, edge, 100, 255, THRESH_BINARY);
        edge.copyTo(edge1);
        edge.copyTo(edge2);
        ImageShow::showImage(edge, scale, "edge0");

        cv::Mat ROI, cross, circle;
        for (int i = 0; i < edge.cols; i++){      
            for (int j = 0; j < edge.rows; j++){
                if( (i >=4) && (i < edge.cols-5) &&(j >=4) && (j < edge.cols-5)){
                    double meanGrayValue_cross_m = 0, meanGrayValue_cross_n = 0;
                    double meanGrayValue_circle = 0;
                    cv::Rect2f SearchArea(i-4, j-4, 9, 9);
                    SearchArea = ImageProcess::RectROI(edge, SearchArea);
                    ROI = ImageProcess::getRectROI(edge, SearchArea);
                    for (int m = 0; m < ROI.rows; m++){
                        uchar* data = ROI.ptr<uchar>(m);
                        for (int n = 0; n < ROI.cols; n++){
                            int intensity = data[n];
                            if (m == 4 || m == 3 || m == 5){
                                if ((m+1)*(n+1)!=25){
                                    meanGrayValue_cross_m = meanGrayValue_cross_m + (double)intensity/8.;
                                }
                            }
                            if (n == 4 || n == 3 || n == 5){
                                if ((m+1)*(n+1)!=25){
                                    meanGrayValue_cross_n = meanGrayValue_cross_n + (double)intensity/8.;
                                }
                            }
                            if((m+1)*(n+1)!=25){
                                meanGrayValue_circle = meanGrayValue_circle + (double)intensity/80.;
                            }
                        }
                    }
                
                    meanGrayValue_cross_m = meanGrayValue_cross_m/255.;
                    meanGrayValue_cross_n = meanGrayValue_cross_n/255.;
                    meanGrayValue_circle = meanGrayValue_circle/255.;
                    if ( ((meanGrayValue_cross_m >= (1-3./8.)) &&  (meanGrayValue_cross_m <= (1)))
                    || ((meanGrayValue_cross_n >= (1-3./8.)) &&  (meanGrayValue_cross_n <= (1)))){
                        if (((meanGrayValue_circle < (0.1-6./80.)) &&  (meanGrayValue_circle < (0.1+6./80.)))
                        ||  ((meanGrayValue_circle < (0.3-6./80.)) &&  (meanGrayValue_circle < (0.3+6./80.)))
                        ||  ((meanGrayValue_circle < (0.5-6./80.)) &&  (meanGrayValue_circle < (0.5+6./80.)))){
                            edge1.at<uchar>(j,i) = 255;
                            edge2.at<uchar>(j,i) = 255;
                        }
                        else{
                            edge1.at<uchar>(j,i) = 0;
                            edge2.at<uchar>(j,i) = 0;
                        }
                    }
                    else{   
                        edge1.at<uchar>(j,i) = 0;
                        edge2.at<uchar>(j,i) = 0;
                    }

                    int dis_x = 101;
                    int dis_y = 101;
                    cv::Rect2f _Area(i-(int)dis_x/2, j-(int)dis_y/2, dis_x, dis_y);
                    _Area = ImageProcess::RectROI(edge, _Area);
                    ROI = ImageProcess::getRectROI(edge, _Area);
                    int cx = ROI.cols / 2;
                    int cy =ROI.rows / 2; 
                    cv::Mat q0(ROI, cv::Rect(0, 0, cx-2, cy-2));
                    cv::Mat q1(ROI, cv::Rect(cx+2, 0, cx-2, cy-2));
                    cv::Mat q2(ROI, cv::Rect(0, cy+2, cx-2, cy-2));
                    cv::Mat q3(ROI, cv::Rect(cx+2, cy+2, cx-2, cy-2));

                    double meanGrayValue_q0 = ImageProcess::getImageMeanGrayValue(q0);
                    double meanGrayValue_q1 = ImageProcess::getImageMeanGrayValue(q1);
                    double meanGrayValue_q2 = ImageProcess::getImageMeanGrayValue(q2);
                    double meanGrayValue_q3 = ImageProcess::getImageMeanGrayValue(q3);

                    if (meanGrayValue_q0*meanGrayValue_q1*meanGrayValue_q2*meanGrayValue_q3 > 0.){
                        edge2.at<uchar>(j,i) = 0;
                        //std::cout<<"i:"<<i<<", j:"<<j<<std::endl;
                    }
                }
                else{
                    edge1.at<uchar>(j,i) = 0;
                    edge2.at<uchar>(j,i) = 0;
                }
            }  
        }


        std::vector<Vec4i> linesP1;
        cv::HoughLinesP(edge2, linesP1, 1, CV_PI / 180, 30, 100, 100);  
        double DelAngle = 45;
        std::vector<cv::Vec4i> lines_horizon, lines_vertical;
	    for (int i = 0; i < linesP1.size(); ++i) {
		    cv::Vec4i line_ = linesP1[i];
		    double k = (double)(line_[3] - line_[1]) / (double)(line_[2] - line_[0]);
		    double angle = atan(k) * 180 / CV_PI;
		    
		    if ((angle >= 0.-DelAngle && angle <= 0.+DelAngle) || (angle >= 360.-DelAngle && angle <= 360.+DelAngle) 
            || (angle >= 180.-DelAngle && angle <= 180.+DelAngle) || (angle >= -180.-DelAngle && angle <= -180.+DelAngle)){
			    lines_horizon.push_back(line_);
		    }
		    else if ((angle >= 90.-DelAngle && angle <= 90.+DelAngle) || (angle >= -90.-DelAngle && angle <= -90.+DelAngle) 
            || (angle >= 270.-DelAngle && angle <= 270.+DelAngle) || (angle >= -270.-DelAngle && angle <= -270.+DelAngle)){
			    lines_vertical.push_back(line_);
		    }  
        } 

		double k_hor = (double)(lines_horizon[0][3] - lines_horizon[0][1]) / (double)(lines_horizon[0][2] - lines_horizon[0][0]);
        double angle_hor = atan(k_hor);
		double k_ver = (double)(lines_vertical[0][3] - lines_vertical[0][1]) / (double)(lines_vertical[0][2] - lines_vertical[0][0]);
        double angle_ver = atan(k_ver);  

        cv::Mat img1;
        srcImage.copyTo(img1);
        double s = 2000;
        cv::line(img1, cv::Point(lines_horizon[0][2]+s*cos(angle_hor), lines_horizon[0][3]+s*sin(angle_hor)), cv::Point(lines_horizon[2][3]-s*cos(angle_hor) , lines_horizon[0][1]-s*sin(angle_hor)), cv::Scalar(0, 0, 255), 2);
        cv::line(img1, cv::Point(lines_vertical[0][2]+s*cos(angle_ver), lines_vertical[0][3]+s*sin(angle_ver)), cv::Point(lines_vertical[0][2]-s*cos(angle_ver), lines_vertical[0][3]-s*sin(angle_ver)), cv::Scalar(0, 0, 255), 2);
        ImageShow::showImage(img1, scale, "img1");


    

        //std::cout<<edge1.cols<<", "<<edge1.rows<<std::endl;
        ImageShow::showImage(edge1, scale, "edge1");
        ImageShow::showImage(edge2, scale, "edge2");
        
    }


   
	if (imageFiles.size() != 0){
        waitKey(0);
	}
    return 0;
}