#include <iostream>
#include <fstream>
#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include <opencv2/core/persistence.hpp>
#include "ImageProcess.h"
#include "ImageLoading.h"

int main(){
    
    cv::Mat src = cv::imread("/Users/xinhui/Desktop/nice.jpg");
    cv::Mat src_Gray;
    cv::cvtColor(src, src_Gray, cv::COLOR_RGB2GRAY);
    //cv::normalize(src_Gray, src_Gray, 0, 255, cv::NORM_MINMAX);

    cv::Mat dst;
    cv::resize(src_Gray, dst, cv::Size(256, 256));
    //cv::imshow("src", src_Gray);

    std::cout<<dst.cols<<", "<<dst.rows<< ", type: "<<dst.type()<<std::endl;
    std::cout<<dst.at<int>(128, 128)<<std::endl;

    std::string filename = "/Users/xinhui/Desktop/image.txt";
    std::ofstream fout(filename);

    for (int i = 0; i < dst.rows; i++){
        uchar* data = dst.ptr<uchar>(i);
		for (int j = 0; j < dst.cols; j++){
            int intensity = data[j];
			fout << i << "\t" << j <<"\t" << intensity <<"\t";
		    fout << std::endl;
		}
	}

	fout.close();

    //cv::waitKey(0);
    return 0;
}


