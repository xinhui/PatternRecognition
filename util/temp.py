def gaussianFreqFilter(img,sigma,centX=None,centY=None):
    fft = np.fft.fft2(img)
    fft = np.fft.fftshift(fft)  # 图像傅里叶变换并移到图像中央位置
 
    # 构造高斯核
    width,height = img.shape
    # if centX is None and centY is None:
    #     centX = int(height/2)
    #     centY = int(width/2)
    Gauss_map1 = np.zeros((width,height))
    centX1 = centX
    centY1 = int(width/2)
    for i in range(width):
        for j in range(height):
            dis = np.sqrt((i - centY1) ** 2 + (j - centX1) ** 2)
            Gauss_map1[i, j] = 1.0 -np.exp(-0.5 * dis / sigma)  # 1.0- 表明是高通滤波器
 
    Gauss_map2 = np.zeros((width,height))
    centX2 = height - centX1
    centY2 = int(width/2)
    for i in range(width):
        for j in range(height):
            dis = np.sqrt((i - centY2) ** 2 + (j - centX2) ** 2)
            Gauss_map2[i, j] = 1.0 - np.exp(-0.5 * dis / sigma)

Gauss_map3 = np.zeros((width,height))
    centX3 = int(height/2)
    centY3 = centY
    for i in range(width):
        for j in range(height):
            dis = np.sqrt((i - centY3) ** 2 + (j - centX3) ** 2)
            Gauss_map3[i, j] = 1.0 - np.exp(-0.5 * dis / sigma)
 
    Gauss_map4 = np.zeros((width,height))
    centX4 = int(height/2)
    centY4 = width - centY3
 
    for i in range(width):
        for j in range(height):
            dis = np.sqrt((i - centY4) ** 2 + (j - centX4) ** 2)
            Gauss_map4[i, j] = 1.0 - np.exp(-0.5 * dis / sigma)
 
 
    blur_fft = fft*Gauss_map1*Gauss_map2*Gauss_map3*Gauss_map4
    blur_img = np.fft.ifft2(blur_fft)
    blur_img = np.abs(blur_img)
 
    return blur_img
