# CMakeLists.txt

cmake_minimum_required(VERSION 2.8)
project( PatternRecognition )

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} ${PROJECT_SOURCE_DIR}/Headers)

file(GLOB_RECURSE PatternRecognition_srcs src/*.cxx)
add_library(PatternRecognitionLib STATIC ${PatternRecognition_srcs})

file(GLOB_RECURSE files "util/*")
foreach(_exeFile ${files})
    get_filename_component(_theExec ${_exeFile} NAME_WE)
    get_filename_component(_theLoc ${_exeFile} DIRECTORY)
    # we specify a folder for programs we do not want to compile. 
    if(${_theLoc} MATCHES "DoNotBuild")
    else()
        set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/build)
      add_executable( ${_theExec} ${_exeFile})
      target_link_libraries( ${_theExec} PUBLIC ${OpenCV_LIBS} PatternRecognitionLib)
    endif()
endforeach()

